﻿// Copyright © 2015 Pavel Tsurbeleu

namespace jwtdecode.generic.tests
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Common test suite to validate general functionality.
    /// </summary>
    [TestClass]
    public class CommonTestSuite
    {
        /// <summary>
        /// Validates the token has been parsed into a prettified string.
        /// </summary>
        [TestMethod]
        public void ShouldParseTokenIntoPrettifiedString()
        {
            // Arrange
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.None);

            // Act
            var printable = Tool.Convert(jwtEncodedString);
            var entries = printable.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            // Assert
            Assert.AreEqual("header ~> {", entries[0]);
            Assert.AreEqual("}", entries[4]);
            Assert.AreEqual("payload ~> {", entries[5]);
            Assert.AreEqual("}", entries[29]);
        }

        /// <summary>
        /// Validates the token has been parsed into sections as expected.
        /// </summary>
        [TestMethod]
        public void ShouldParseTokenIntoSections()
        {
            // Arrange
            var expected = 2;
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.Identity);

            // Act
            var printable = this.Act(jwtEncodedString);
            var actual = printable.Sections.Count;

            // Assert
            Assert.AreEqual<int>(expected, actual);
        }

        /// <summary>
        /// Validates whether 'Bearer' token scheme is supported.
        /// </summary>
        [TestMethod]
        public void ShouldSupportBearerTokenScheme()
        {
            // Arrange
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.Bearer);

            // Act
            var printable = this.Act(jwtEncodedString);

            // Assert
            Assert.AreEqual<string>("bearer", printable.TypeString);
        }

        /// <summary>
        /// Validates whether 'Bearer' token scheme support is not case-sensitive.
        /// </summary>
        [TestMethod]
        public void ShouldIgnoreCaseInBearerTokenScheme()
        {
            // Arrange
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.BeARer);

            // Act
            var printable = this.Act(jwtEncodedString);

            // Assert
            Assert.AreEqual<string>("bearer", printable.TypeString);
        }

        /// <summary>
        /// Validates whether 'access_token" token scheme is supported.
        /// </summary>
        [TestMethod]
        public void ShouldSupportAccessTokenScheme()
        {
            // Arrange
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.Access);

            // Act
            var printable = this.Act(jwtEncodedString);

            // Assert
            Assert.AreEqual<string>("access_token", printable.TypeString);
        }

        /// <summary>
        /// Validates whether 'id_token' token scheme is supported.
        /// </summary>
        [TestMethod]
        public void ShouldSupportIdentityTokenScheme()
        {
            // Arrange
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.Identity);

            // Act
            var printable = this.Act(jwtEncodedString);

            // Assert
            Assert.AreEqual<string>("id_token", printable.TypeString);
        }

        /// <summary>
        /// Validates whether 'id_token' or 'access_token' token schemes tolerate extra specing around token scheme string.
        /// </summary>
        [TestMethod]
        public void ShouldIgnoreSpacingInAccessOrIdentityTokenScheme()
        {
            // Arrange
            var jwtEncodedString = TestConstants.GetFormattedTokenSchemeString(TokenFormatScheme.AccessOrIdentityVolatile);

            // Act
            var printable = this.Act(jwtEncodedString);

            // Assert
            Assert.AreEqual<string>("access_token", printable.TypeString);
        }

        /// <summary>
        /// Executes the given payload against system-under-test.
        /// </summary>
        private Printable Act(string jwtEncodedString)
        {
            var printable = Tool.Convert(jwtEncodedString);
            return printable;
        }
    }
}
