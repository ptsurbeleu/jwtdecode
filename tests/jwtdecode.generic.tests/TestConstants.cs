﻿// Copyright © 2015 Pavel Tsurbeleu.

namespace jwtdecode.generic.tests
{
    /// <summary>
    /// Helper class to co-locate fixed strings and avoid duplication.
    /// </summary>
    internal class TestConstants
    {
        public const string jwtEncodedString = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik1uQ19WWmNBVGZNNXBPWWlKSE1iYTlnb0VLWSJ9.eyJhdWQiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8wMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJpYXQiOiIxNDI4MDM2NTM5IiwibmJmIjoiMTQyODAzNjUzOSIsImV4cCI6IjE0MjgwNDA0MzkiLCJ2ZXIiOiIxLjAiLCJ0aWQiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJhbXIiOiJwd2QiLCJvaWQiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJlbWFpbCI6Impkb2VAbGl2ZS5jb20iLCJwdWlkIjoiSm9obiBEb2UiLCJpZHAiOiJsaXZlLmNvbSIsImFsdHNlY2lkIjoiMTpsaXZlLmNvbTowMDAwMDAwMDAwMDAwMDAwIiwic3ViIjoieHh4eHh4eHh4eHh4eHh4eC15eXl5eSIsImdpdmVuX25hbWUiOiJKb2huIiwiZmFtaWx5X25hbWUiOiJEb2UiLCJuYW1lIjoiSm9obiBEb2UiLCJncm91cHMiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJ1bmlxdWVfbmFtZSI6ImxpdmUuY29tI2pkb2VAbGl2ZS5jb20iLCJhcHBpZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImFwcGlkYWNyIjoiMCIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsImFjciI6IjEifQ.K7BCa0NO-A5f9exFiWcIXFMGnLmmt3V2HVP0itMT-GsAxnQROWzJFDIQNFo4QhiW0NCCqJykVELeVBCy_7Dex2-szUPZ69rmmDVJhy_qkmAiHhS1mNZDvJ1sB-whb5wOJ_QPIlByVzubhTcNnuliTVjnTeuOurVJJcn0Vugx9UDkGgky0etHXzmKukWYp4nzA68Wf1xnzlMZBz7PfoPGhjgzQfceOkZJVXIBRMB_7tsyW7gYNbHB_aTiT47cEjkh-UdrZEdp2UaAKugC-es3m076kRHMJqx31x-zDLDBttKinRJVPctiqwb1jMOMV6cUAp2E6aMfEbNk_iqX_OKFJg";

        /// <summary>
        /// Gets token string using the formatted scheme specified.
        /// </summary>
        /// <param name="scheme">Token scheme to use for formatting purposes.</param>
        /// <returns>Formatted token string.</returns>
        public static string GetFormattedTokenSchemeString(TokenFormatScheme scheme)
        {
            var tokenString = jwtEncodedString;

            switch (scheme)
            {
                case TokenFormatScheme.Identity:
                    tokenString = "id_token=" + tokenString;
                    break;
                case TokenFormatScheme.Bearer:
                    tokenString = "Bearer " + tokenString;
                    break;
                case TokenFormatScheme.BeARer:
                    tokenString = "BeARer " + tokenString;
                    break;
                case TokenFormatScheme.Access:
                    tokenString = "access_token=" + tokenString;
                    break;
                case TokenFormatScheme.AccessOrIdentityVolatile:
                    tokenString = "access_token = " + tokenString;
                    break;
            }

            return tokenString;
        }
    }

    /// <summary>
    /// Token formatting scheme enumeration.
    /// </summary>
    enum TokenFormatScheme
    {
        /// <summary>
        /// Formatting scheme is not defined, eq.: , eq.: "eyJ0eXAiOiJKV1Qi...".
        /// </summary>
        None = 0,

        /// <summary>
        /// Identity token formatting scheme, eq.: "id_token=eyJ0eXAiOiJKV1Qi...".
        /// </summary>
        Identity,

        /// <summary>
        /// Bearer token formatting scheme, eq.: "Bearer eyJ0eXAiOiJKV1Qi...".
        /// </summary>
        Bearer,

        /// <summary>
        /// BeARer token formatting scheme, eq.: "BeARer eyJ0eXAiOiJKV1Qi...".
        /// </summary>
        BeARer,

        /// <summary>
        /// Access token formatting scheme, eq.: "access_token=eyJ0eXAiOiJKV1Qi...".
        /// </summary>
        Access,

        /// <summary>
        /// Access or identity token formatting scheme with extra spaces around the delimiter, eq.: "access_token = eyJ0eXAiOiJKV1Qi...".
        /// </summary>
        AccessOrIdentityVolatile
    }
}