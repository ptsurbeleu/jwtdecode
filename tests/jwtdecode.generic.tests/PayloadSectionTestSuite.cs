﻿// Copyright © 2015 Pavel Tsurbeleu

namespace jwtdecode.generic.tests
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Summary description for PayloadSectionTestSuite
    /// </summary>
    [TestClass]
    public class PayloadSectionTestSuite
    {
        /// <summary>
        /// Validates the start of the token payload section has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyStartOfSectionAsExpected()
        {
            // Arrange
            var expected = "payload ~> {";

            // Act
            var actual = this.Act()[0];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'aud' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyAudienceClaimAsExpected()
        {
            // Arrange
            var expected = "  \"aud\": \"00000000-0000-0000-0000-000000000000\",";

            // Act
            var actual = this.Act()[1];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'iss' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyIssuerClaimAsExpected()
        {
            // Arrange
            var expected = "  \"iss\": \"https://sts.windows.net/00000000-0000-0000-0000-000000000000\",";

            // Act
            var actual = this.Act()[2];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'iat' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyIssuedAtClaimAsExpected()
        {
            // Arrange
            var expected = "  \"iat\": \"1428036539\" --> (4/2/2015 9:48:59 PM),";

            // Act
            var actual = this.Act()[3];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'nbf' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyNotBeforeClaimAsExpected()
        {
            // Arrange
            var expected = "  \"nbf\": \"1428036539\" --> (4/2/2015 9:48:59 PM),";

            // Act
            var actual = this.Act()[4];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'exp' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyExpiresClaimAsExpected()
        {
            // Arrange
            var expected = "  \"exp\": \"1428040439\" --> (4/2/2015 10:53:59 PM),";

            // Act
            var actual = this.Act()[5];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'ver' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyVersionClaimAsExpected()
        {
            // Arrange
            var expected = "  \"ver\": \"1.0\",";

            // Act
            var actual = this.Act()[6];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'tid' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyTenantIdentifierClaimAsExpected()
        {
            // Arrange
            var expected = "  \"tid\": \"00000000-0000-0000-0000-000000000000\",";

            // Act
            var actual = this.Act()[7];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'amr' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyAuthenticationMethodsReferencesClaimAsExpected()
        {
            // Arrange
            var expected = "  \"amr\": \"pwd\",";

            // Act
            var actual = this.Act()[8];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'oid' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyObjectIdentifierClaimAsExpected()
        {
            // Arrange
            var expected = "  \"oid\": \"00000000-0000-0000-0000-000000000000\",";

            // Act
            var actual = this.Act()[9];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'email' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyEmailClaimAsExpected()
        {
            // Arrange
            var expected = "  \"email\": \"jdoe@live.com\",";

            // Act
            var actual = this.Act()[10];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'puid' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyPuidClaimAsExpected()
        {
            // Arrange
            var expected = "  \"puid\": \"John Doe\",";

            // Act
            var actual = this.Act()[11];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'idp' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyIdentityProviderClaimAsExpected()
        {
            // Arrange
            var expected = "  \"idp\": \"live.com\",";

            // Act
            var actual = this.Act()[12];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'altsecid' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyAlternativeSecurityIdentifierClaimAsExpected()
        {
            // Arrange
            var expected = "  \"altsecid\": \"1:live.com:0000000000000000\",";

            // Act
            var actual = this.Act()[13];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'sub' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifySubjectClaimAsExpected()
        {
            // Arrange
            var expected = "  \"sub\": \"xxxxxxxxxxxxxxxx-yyyyy\",";

            // Act
            var actual = this.Act()[14];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'given_name' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyGivenNameClaimAsExpected()
        {
            // Arrange
            var expected = "  \"given_name\": \"John\",";

            // Act
            var actual = this.Act()[15];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'family_name' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyFamilyNameClaimAsExpected()
        {
            // Arrange
            var expected = "  \"family_name\": \"Doe\",";

            // Act
            var actual = this.Act()[16];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'name' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyNameClaimAsExpected()
        {
            // Arrange
            var expected = "  \"name\": \"John Doe\",";

            // Act
            var actual = this.Act()[17];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'groups' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyGroupsClaimAsExpected()
        {
            // Arrange
            var expected = "  \"groups\": \"00000000-0000-0000-0000-000000000000\",";

            // Act
            var actual = this.Act()[18];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'unique_name' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyUniqueNameClaimAsExpected()
        {
            // Arrange
            var expected = "  \"unique_name\": \"live.com#jdoe@live.com\",";

            // Act
            var actual = this.Act()[19];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'appid' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyApplicationIdentifierClaimAsExpected()
        {
            // Arrange
            var expected = "  \"appid\": \"00000000-0000-0000-0000-000000000000\",";

            // Act
            var actual = this.Act()[20];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'appidacr' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyApplicationIdentifierAuthenticationContextClassReferenceClaimAsExpected()
        {
            // Arrange
            var expected = "  \"appidacr\": \"0\",";

            // Act
            var actual = this.Act()[21];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'scp' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyScopeClaimAsExpected()
        {
            // Arrange
            var expected = "  \"scp\": \"user_impersonation\",";

            // Act
            var actual = this.Act()[22];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token payload 'acr' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyAuthenticationContextClassReferenceClaimAsExpected()
        {
            // Arrange
            var expected = "  \"acr\": \"1\"";

            // Act
            var actual = this.Act()[23];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the end of the the token payload section has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyEndOfSectionAsExpected()
        {
            // Arrange
            var expected = "}";

            // Act
            var actual = this.Act()[24];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        private string[] Act()
        {
            var printable = Tool.Convert(TestConstants.jwtEncodedString);
            var section = printable.Sections[1];
            return section.PrettyPrint.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
