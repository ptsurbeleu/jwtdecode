﻿// Copyright © 2015 Pavel Tsurbeleu

namespace jwtdecode.generic.tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Header section test suite to validate jwt header rendering style.
    /// </summary>
    [TestClass]
    public class HeaderSectionTestSuite
    {
        /// <summary>
        /// Validates the end of the token header section has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyEndOfSectionAsExpected()
        {
            // Arrange
            var expected = "}";

            // Act
            var actual = this.Act()[4];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token header 'x5t' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyThumbprintHashClaimAsExpected()
        {
            // Arrange
            var expected = "  \"x5t\": \"MnC_VZcATfM5pOYiJHMba9goEKY\"";

            // Act
            var actual = this.Act()[3];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token header 'alg' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyAlgorithmClaimAsExpected()
        {
            // Arrange
            var expected = "  \"alg\": \"RS256\",";

            // Act
            var actual = this.Act()[2];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the token header 'typ' claim has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyTypeClaimAsExpected()
        {
            // Arrange
            var expected = "  \"typ\": \"JWT\",";

            // Act
            var actual = this.Act()[1];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Validates the start of the token header section has been rendered as expected.
        /// </summary>
        [TestMethod]
        public void ShouldPrettifyStartOfSectionAsExpected()
        {
            // Arrange
            var expected = "header ~> {";

            // Act
            var actual = this.Act()[0];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        private string[] Act()
        {
            var printable = Tool.Convert(TestConstants.jwtEncodedString);
            var section = printable.Sections[0];
            return section.PrettyPrint.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
