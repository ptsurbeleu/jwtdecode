@echo off
@call setlocal

@if exist "%ProgramFiles(x86)%\MSBuild\12.0" set "MSBUILD=%ProgramFiles(x86)%\MSBuild\12.0\Bin\MSBuild.exe"

@if "%MSBUILD%" == "" (
    @echo "ERROR: Cannot find MSBuild on your machine in %ProgramFiles(x86)%\MSBuild\12.0\Bin\."
    @goto end
)

@set MSBUILD_FL=logfile=build.log

@rem This block allows to parse all parameters without a specific order
:parse
@if "%~1" == "" (
    @goto endparse
)

@rem Enable release configuration for the current build run
@if "%~1" == "--release" (
    @set ENVY_RELEASE=envy [build] - Executed the build with RELEASE configuration
    @set __ARGS=/property:Configuration=Release
)

@rem Enable diagnostic verbosity for the current build run
@if "%~1" == "--diagnostic" (
    @set MSBUILD_FL=%MSBUILD_FL%;verbosity=diagnostic
)

@rem Enable build signig for the current build run
@if "%~1" == "--sign" (
    @set ENVY_SIGN=envy [build] - Executed the build and signed the output bits, you can use 'sn -Tp path_to_file' command to validate the signature...
    @set __ARGS=/property:SignedBuild=true
)

@rem Enable NuGet package assembly for the current build run
@if "%~1" == "--package" (
    @set ENVY_PACKAGE=envy [build] - Executed build with automatically generated NuGet package from the project's build artefacts...
    @set __ARGS=/property:BuildPackage=true /property:NuGetExePath=%REPOROOT%\.envy\nuget.exe
)

@if not "%__ARGS%" == "" (
    @set MSBUILD_ARGS=%MSBUILD_ARGS% %__ARGS%
    @set __ARGS=
)

@shift
@goto parse
:endparse


@rem Restore all NuGet packages (to ensure the build has all the dependencies)
@call restore

@rem Build the sources
@rem See the following link for detailed command-line arguments reference -> https://msdn.microsoft.com/en-us/library/ms164311.aspx
@call "%MSBUILD%" /m /fl /fl1 /fl2 /flp:%MSBUILD_FL% /flp1:logfile=errors.log;errorsonly /flp2:logfile=warnings.log;warningsonly %MSBUILD_ARGS%

@rem Give some useful hints to the developer
@if not "%ENVY_RELEASE%" == "" (
    @echo.
    @echo %ENVY_RELEASE%
)

@if not "%ENVY_PACKAGE%" == "" (
    @echo.
    @echo %ENVY_PACKAGE%
)

@rem Give some useful hints to the developer
@if "%MSBUILD_FL%" == "logfile=build.log;verbosity=diagnostic" (
    @echo.
    @echo envy [build] - Executed with diagnostic verbosity for troubleshooting purposes, see build.log for more info...
)

@rem Give some useful hints to the developer
@if not "%ENVY_SIGN%" == "" (
    @echo.
    @echo %ENVY_SIGN%
)

:end
@call endlocal