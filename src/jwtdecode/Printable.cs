﻿// Copyright © 2015 Pavel Tsurbeleu

namespace jwtdecode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Printable
    {
        public bool HasErrors
        {
            get { return this.Sections.Any(s => s.PrettyPrint.StartsWith("error")); }
        }

        public string TypeString { get; private set; }

        public List<PrintableSection> Sections { get; private set; }

        public Printable(string typeString)
        {
            this.TypeString = typeString;
            this.Sections = new List<PrintableSection>();
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine + Environment.NewLine, this.Sections.Select(s => s.PrettyPrint));
        }
    }
}
