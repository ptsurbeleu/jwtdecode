﻿// Copyright (c) Pavel Tsurbeleu

namespace jwtdecode
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Bootstrap helper class to handle loading assemblies from the embedded resources.
    /// </summary>
    static class Bootstrap
    {
        /// <summary>
        /// Subscribes to resolve assembly requests from the current application domain.
        /// </summary>
        public static void SetupAssemblyResolveInterception()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        /// <summary>
        /// Handles AssemblyResolve event raised by the current application domain to load assemblies from embedded resources.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The event data.</param>
        /// <returns>The assembly that resolves the type, assembly, or resource; or null if the assembly cannot be resolved.</returns>
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            // TODO: This snippet should return null if no resource key is found in the assembly.
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(args.Name))
            {
                var buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                return Assembly.Load(rawAssembly: buffer);
            }
        }
    }
}
