﻿// Copyright © 2015 Pavel Tsurbeleu

namespace jwtdecode
{
    public class PrintableSection
    {
        public string PrettyPrint { get; private set; }

        public PrintableSection(string prettyPrint)
        {
            this.PrettyPrint = prettyPrint;
        }
    }
}
