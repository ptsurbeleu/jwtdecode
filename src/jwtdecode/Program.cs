﻿// Copyright (c) Pavel Tsurbeleu

namespace jwtdecode
{
    using System;
    using System.Net.Http;
    using System.Reflection;

    class Program
    {
        static Program()
        {
            // The following post has very helpful hints how to address some of the issues with resolving assemblies correctly:
            // http://www.aboutmycode.com/net-framework/assemblyresolve-event-tips/
            Bootstrap.SetupAssemblyResolveInterception();
        }

        /// <remarks>Windows Forms is not supported within a MTA or free threaded apartment. 
        /// Applications using Windows Forms should always declare the apartment style they're using, as some other 
        /// component could initialize the apartment state of thread improperly.
        /// See http://blogs.msdn.com/b/jfoscoding/archive/2005/04/07/406341.aspx for further details.</remarks>
        [STAThread]
        static void Main(string[] args)
        {
            var options = Options.ParseFromArguments(args);

            ShowAboutInfo();

            if (options.Demo)
            {
                Console.WriteLine("This is an example jwt-encoded string used for demonstration purposes of the tool: " + options.JwtEncodedString);
            }

            if (string.IsNullOrWhiteSpace(options.JwtEncodedString))
            {
                ShowHelpInfo();
                return;
            }

            // Build a printable object out of the specified jwt-encoded token string
            var printable = Tool.Convert(options.JwtEncodedString);

            // Wrap the output with new line to avoid content overlapping
            Console.WriteLine();
            Console.WriteLine(printable);

            // Do not send stats if we've been asked not to or of there is an error
            if (printable.HasErrors) { return; }
            if (options.Incognito)   { return; }
            if (options.Demo)        { return; }

            SendStats();
        }

        private static void ShowHelpInfo()
        {
            var templateString = @"usage: jwtdecode [--incognito] [--demo] [--clipboard] <eyJ0eXAiOiJKV1...>

available options:

   clipboard       Decode a jwt-encoded string using content from the clipboard
   demo            Print out an example jwt-encoded token string to demonstrate how the tool works
   incognito       Disables sending stats during the current run (can be combined with clipboard option)

examples:

   jwtdecode eyJ0eXAiOiJKV1Qi...                    Decodes a jwt-encoded string using the input string specified
   jwtdecode --incognito eyJ0eXAiOiJKV1Qi...        Decodes a jwt-encoded string using the input string specified and prevents the tool from sending any usage stats
   jwtdecode --clipboard                            Decodes a jwt-encoded string using content from the clipboard
   jwtdecode --incognito --clipboard                Decodes a jwt-encoded string using content from the clipboard and prevents the tool from sending any usage stats
   jwtdecode --demo                                 Prints an example of jwt-encoded token string to demonstrate how the tool works";
            Console.WriteLine(templateString);
        }

        private static void ShowAboutInfo()
        {
            var assemblyName = new AssemblyName(Assembly.GetEntryAssembly().FullName);
            Console.WriteLine();
            Console.WriteLine("JWT-Encoded Token Conversion Tool.  Version " + assemblyName.Version);
            Console.WriteLine("Copyright (c) Pavel Tsurbeleu.  All rights reserved.");
            Console.WriteLine();
        }

        private static void SendStats()
        {
            // Sketch of the tool's usage counter stats using bit.ly and version-specific referer to track version-specific usage of the tool.
            var hc = default(HttpClient);

            try
            {
                hc = new HttpClient();
                hc.DefaultRequestHeaders.Add("User-Agent", "jwtdecode/1.0.1");
                hc.DefaultRequestHeaders.Add("Referer", "https://bitbucket.org/ptsurbeleu/jwtdecode/1.0.1");
                hc.Timeout = TimeSpan.FromSeconds(2);
                hc.PostAsync("http://bit.ly/jwtd-proj", content: null).Wait();
            }
            catch { /* TODO: We should trace exception into the console if --verbose mode is enabled */ }
            finally
            {
                hc.Dispose();
            }
        }
    }
}