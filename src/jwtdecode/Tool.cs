﻿// Copyright (c) 2015 Pavel Tsurbeleu

namespace jwtdecode
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IdentityModel.Tokens;
    using System.Text;

    public class Tool
    {
        public static Printable Convert(string jwtEncodedString)
        {
            var typeString = "";
            if (jwtEncodedString.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                typeString = "bearer";
                jwtEncodedString = jwtEncodedString.Substring(7);
            }

            var indexOf = jwtEncodedString.IndexOf('=');
            if (indexOf > -1)
            {
                // trim type string
                typeString = jwtEncodedString.Substring(0, indexOf);
                typeString = typeString.Trim();

                // trim jwt string as well
                jwtEncodedString = jwtEncodedString.Substring(indexOf + 1);
                jwtEncodedString = jwtEncodedString.Trim();
            }

            var printable = new Printable(typeString);
            var sections = printable.Sections;
            var jwt = default(JwtSecurityToken);

            try
            {
                jwt = new JwtSecurityToken(jwtEncodedString);

                if (jwt.Header != null)
                {
                    var prettyString = PrettifyString(label: "header", source: Convert(jwt.Header));
                    var section = new PrintableSection(prettyPrint: prettyString);
                    sections.Add(section);
                }

                if (jwt.Payload != null)
                {
                    var prettyString = PrettifyString(label: "payload", source: Convert(jwt.Payload));
                    sections.Add(new PrintableSection(prettyPrint: prettyString));
                }
            }
            catch (Exception ex)
            {
                var prettyString = PrettifyErrorString(ex);
                sections.Add(new PrintableSection(prettyPrint: prettyString));
            }

            return printable;
        }

        /// <summary>
        /// Converts a dictionary with claims into name-value collection for further processing.
        /// </summary>
        private static NameValueCollection Convert(Dictionary<string, object> claims)
        {
            var nv = new NameValueCollection();
            foreach (var name in claims.Keys)
            {
                nv.Add(name, System.Convert.ToString(claims[name]));
            }

            return nv;
        }

        /// <summary>
        /// Gets prettified version of an exception specified in the form "error ~> something went wrong...".
        /// </summary>
        private static string PrettifyErrorString(Exception ex)
        {
            var prefix = PrettifyLabel(label: "error");
            var prettyString = new StringBuilder(prefix);

            int length = 0;
            foreach (char c in ex.Message)
            {
                // proceed to the closest ' ' (whitespace) symbol when the length is exceeded
                if (length >= 80 && c == ' ')
                {
                    // continue on a new line, skipping the whitespace
                    prettyString.AppendLine();
                    length = 0;
                    continue;
                }

                prettyString.Append(c);
                length++;
            }

            return prettyString.ToString();
        }

        private static string PrettifyLabel(string label)
        {
            return label + " ~> ";
        }

        private static string PrettifyString(string label, NameValueCollection source)
        {
            var prefix = PrettifyLabel(label: label);
            var prettyString = new StringBuilder(prefix);
            var prettyClaims = new List<string>();

            foreach (string name in source.Keys)
            {
                // Pre-process claim value to make it human-friendly
                var valueStr = GetHumanFriendlyClaimValue(name, source[name]);
                prettyClaims.Add(string.Format("  \"{0}\": {1}", name, valueStr));
            }

            // Make json-formatted payload
            prettyString.AppendFormat("{0}{1}", "{", Environment.NewLine);
            prettyString.Append(string.Join("," + Environment.NewLine, prettyClaims));
            prettyString.AppendFormat("{0}{1}", Environment.NewLine, "}");

            return prettyString.ToString();
        }

        private static string GetHumanFriendlyClaimValue(string type, string value)
        {
            if (MatchTimeSpecificClaim(type))
            {
                var reference = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                double totalSecondsExpiresIn = System.Convert.ToDouble(value);
                var time = (reference + TimeSpan.FromSeconds(totalSecondsExpiresIn)).ToLocalTime();
                return string.Format("\"{0}\" --> ({1})", value, time);
            }

            return string.Format("\"{0}\"", value);
        }

        private static bool MatchTimeSpecificClaim(string type)
        {
            return type.Equals("exp", StringComparison.OrdinalIgnoreCase)
                || type.Equals("iat", StringComparison.OrdinalIgnoreCase)
                || type.Equals("nbf", StringComparison.OrdinalIgnoreCase);
        }
    }
}