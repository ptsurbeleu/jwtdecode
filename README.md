# README #

This is an open-source command-line alternative to [http://jwt.io](http://jwt.io).

It's a very simple commad-line utility that decodes a JWT-encoded string (which is usually a security token) into its human-readable representation 
for inspection and troubleshooting purposes and renders it in the console.

Oftentimes, while dealing with [OpenID Connect](http://bit.ly/22PLqvm) and/or [OAuth 2.0](http://bit.ly/1UCftk5) in your application you need to peek inside a JWT-encoded 
security token content issued by an identity provider and this is where jwtdecode comes to the rescue.


### How do I install it? ###

If you're looking for an easy way to install the tool then navigate to [jwtdecode @ chocolatey.org](http://bit.ly/1mKRKmP) to use [chocolatey](https://chocolatey.org) 
(package manager for Windows) to install jwtdecode on your computer.


### Configure the tool ###

No configuration is needed. Just type jwtdecode in your console to get started.


### What does '--incognito' option do? ###

Since the inception of the tool I was captivated by "tokens decoded so far" metric published at jwt.io's front page. So I decided, to use a very simple combination of 
bit.ly + Referrer header to mimic the jwt.io's metric and track (or not to track) number of times the tool has been used.

Being aware of these numbers is a very important insight for me as an author of how the tool is used and more importantly how it can be improved going further.

Thus, I personally want to say thank you to everyone who will participate in this effort of sharing the tool's usage stats!

And I also want to share my sincere appreciation for those who decided to stay '--incognito' but still use the tool, your privacy is of paramount importance for 
me and that's why does '--incognito' option exist in the first place.


### Get help with the tool ###

Send me a tweet if something is not up to par with your expectations or file a bug using 'Issues' link on the left if you encounter any issues.

Hope you will find the tool useful, helpful and easy to use! Enjoy!


Handcrafted in Washington with ❤ by [Pavel Tsurbeleu](http://pabloduo.com) [@pabloduo](https://twitter.com/pabloduo)